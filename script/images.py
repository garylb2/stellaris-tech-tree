#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import imp
from os import listdir, makedirs, path
import os
import shutil
import subprocess

import argparse
import codecs
import re
import sys
import tempfile
from ddsprobe import DDSProbe
from PIL import Image

config = imp.load_source('config', 'config.py')

UTF8Writer = codecs.getwriter('utf8')
sys.stdout = UTF8Writer(sys.stdout)

# Process CLI arguments:
def valid_label(label):
    if not re.match(r'^\w+$', label):
        raise argparse.ArgumentTypeError('Must match [a-z0-9_]')
    elif label not in config.mods.keys():
        raise argparse.ArgumentTypeError('Unsupported mod')
    elif not path.isdir(path.join('public', label)):
        makedirs(path.join('public', label))

    return label.lower()


def valid_dirs(directory):
    if not path.isdir(directory):
        message = "'{}' not found or not a directory".format(directory)
        raise argparse.ArgumentTypeError(message)

    return directory

arg_parser = argparse.ArgumentParser(
    description='Parse Stellaris gfx files and convert')
arg_parser.add_argument('mod', type=valid_label)

args = arg_parser.parse_args()
mod_id = config.mods[args.mod]
tree_label = args.mod
directories = [config.game_dir]
base_output_directory = path.join('public', tree_label, 'img')
#makedirs(base_output_directory)

if type(mod_id) is int:
    mod_dir = path.join(config.workshop_dir, str(mod_id), 'mod')
    directories.append(mod_dir)

def convertTgaToPng(originalFilePath, tgafilePath, output_dir, ffprobeToolPath=''):
    try:
        im = Image.open(tgafilePath)
        #print(desiredFileName, im.format, im.size, im.mode)
        b = path.basename(tgafilePath)
        baseFileNameWOExt, e = path.splitext(b)
        finalFileName = baseFileNameWOExt+'.png'
        abs_output_filepath = path.join(output_dir, finalFileName)

        # verify real pixelFormat of original
        d = DDSProbe()
        d.readFile(originalFilePath)

        #print("FFProbe --", originalFilePath, metadata.streams[0].pixelFormat())
        if d.pixelFormatName == "RGB24":
            # copy and strip alpha
            background = Image.new("RGB", im.size, (255, 255, 255))
            background.paste(im)
            background.save(abs_output_filepath)
        else:
            im.save(abs_output_filepath)

        #print("CONVERTED TO", abs_output_filepath)
    except IOError as ioError:
        print("cannot convert", tgafilePath, ioError)
    except NotImplementedError as e:
        print("unsupported file format", file_path, e)
    else:
        return abs_output_filepath

def convert_all(input_dir, output_dir):
    # shouldn't have issues with possible race condition
    # since this is a manually run operation
    if not path.exists(output_dir):
        os.makedirs(output_dir)

    filepath_to_copy = []
    for filename in listdir(input_dir):
        file_path = path.join(input_dir, filename)
        if not path.isfile(file_path) \
           or not file_path.endswith('.dds'):
            continue
        filepath_to_copy.append(file_path)

    print('Copying {} files to temp dir'.format(len(filepath_to_copy)))

    t = tempfile.mkdtemp(prefix='Stellaris_')
    dds_input_files = []
    for filepath in filepath_to_copy:
        b = path.basename(filepath)
        d = path.join(t,b)
        try:
            shutil.copyfile(filepath, d)
        except IOError:
            print("Error copying", filepath)
        else:
            dds_input_files.append(d)

    print("Files Copied: {}".format(len(dds_input_files)))

    currDir = os.getcwd()
    toolPath = path.join(currDir, 'readdxt.exe')
    os.chdir(t)
    converted_file_paths = []

    for filepath in dds_input_files:
        b = path.basename(filepath)
        subprocess.call([toolPath, b])

        f, e = path.splitext(filepath)
        tmpFileName = f+'00.tga'
        desiredFileName = f+'.tga'
        os.rename(tmpFileName, desiredFileName)
        # print("Converted to ", desiredFileName)

        final_file_path = convertTgaToPng(filepath, desiredFileName, output_dir, ffprobeToolPath=currDir)
        if type(final_file_path) is str:
            converted_file_paths.append(final_file_path)

    print('Converted {} files from {} to {}'.format(len(converted_file_paths), input_dir, output_dir))

    os.chdir(currDir)

    # clean-up temp files
    shutil.rmtree(t)

currWorkingDir = os.getcwd()
for directory in directories:
    print("Converting technology icons")
    temp = path.join(directory,'gfx/interface/icons/technologies')
    abs_output_directory = path.join(currWorkingDir, base_output_directory)
    convert_all(temp, abs_output_directory)

    print("Converting resource icons")
    resource_input = path.join(directory,'gfx/interface/icons/resources')
    resource_output = path.join(currWorkingDir, base_output_directory, 'resources')
    convert_all(resource_input, resource_output)

    print("Converting job icons")
    resource_input = path.join(directory,'gfx/interface/icons/jobs')
    resource_output = path.join(currWorkingDir, base_output_directory, 'jobs')
    convert_all(resource_input, resource_output)

    print("Converting text icons")
    resource_input = path.join(directory,'gfx/interface/icons/text_icons')
    resource_output = path.join(currWorkingDir, base_output_directory, 'text_icons')
    convert_all(resource_input, resource_output)
