import re

nested_key_re = r'\$(\w+)\$'

class Component:
    def __init__(self, component_data, loc_data):
        self.key = self._key(component_data)
        self.name = loc_data.get(self.key, self.key)
        self.prerequisites = self._prerequisites(component_data)

        # deal with nested localizations
        if self.name.find('$') != -1:
            vars = re.findall(nested_key_re,self.name)
            loc = []
            for v in vars:
                t = v.replace('$', '')
                try:
                    t = loc_data[t]
                except KeyError:
                    print('Could NOT find {} key'.format(v))
                    t = v

                loc.append(t)

            self.name = re.sub(nested_key_re,'{}', self.name)
            self.name = self.name.format(*loc)
            re.purge()

    def _key(self, component_data):
        return next(iter(
            subkey for subkey in component_data if subkey.keys()[0] == 'key'
        ))['key']

    def _prerequisites(self, component_data):
        try:
            prerequisites = next(iter(
                subkey for subkey in component_data
                if subkey.keys()[0] == 'prerequisites'
            ))['prerequisites']
        except (StopIteration):
            prerequisites = []

        return prerequisites
